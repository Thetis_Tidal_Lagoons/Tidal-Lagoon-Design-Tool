#ifndef MOVEMENT_H
#define MOVEMENT_H

#include <QPointF>

class Movement
{


public:

  enum {vertexMovement = 1};
  enum {addVertex = 2};
  enum {deleteVertex = 3};
  enum {addEdge = 4};
  enum {deleteEdge = 5};

  Movement();
  Movement(int vertexNo, QPointF oldPosition, QPointF newPosition);
  Movement(int vertexNo, QPointF position, int action);
  Movement(int startVertex, int endVertex, int action);


  //Infomation for vertex actions
  int vertexNumber;
  QPointF oldPos;
  QPointF newPos;

  //Information for edge actions
  int startVertexNumber = 0;
  int endVertexNumber = 0;

  int getActionType();

private:
  int actionType = 0;
};

#endif // MOVEMENT_H
