#ifndef SIDEBAR_H
#define SIDEBAR_H

#include <QWidget>
#include <QGraphicsItemGroup>

#include <memory>

#include "movement.h"

class Mesh;
class Field;
class FieldEditor;

namespace Ui {
  class SideBar;
}

class SideBar : public QWidget
{
  Q_OBJECT

public:
  explicit SideBar(QWidget *parent = 0);
  ~SideBar();

  ///////////////////////////////////////////
  // Changelog functions
  ///////////////////////////////////////////
  void undoChain();

  void redoChain();

  QString getPrependText();

  QString getAppendText();

  void writeUnknownText(QString text);

  FieldEditor* getFieldEditor();


signals:

  void newMeshLoaded(Mesh* currentMesh);

  void meshSizeChanged(int vertexCount);

  ///////////////////////////////////////////
  // Main functions
  ///////////////////////////////////////////

  void cycleViewMode();

  void setViewMode(int viewModeID);

  void activeVertexChanged(int vertexNumber);

  ///////////////////////////////////////////
  // View functions
  ///////////////////////////////////////////


  void setViewRotation(int rotationAngle);


  ///////////////////////////////////////////
  // Changelog functions
  ///////////////////////////////////////////

  void addVertexMovement(int vertexNumber);

  void addVertexInChangelog(int vertexNumber);

  void deleteVertexInChangelog(int vertexNumber);

  void addEdgeInChangelog(int startVertexNumber, int endVertexNumber);

  void deleteEdgeInChangelog(int startVertexNumber, int endVertexNumber);


public slots:

  ///////////////////////////////////////////
  // Mesh Info functions
  ///////////////////////////////////////////

  void updateMeshInfo();

  void refreshVertexCoords(int vertexNumber);

private slots:


  ///////////////////////////////////////////
  // Main functions
  ///////////////////////////////////////////

  /*
   * Reads the given vertex's position and populates the coordinate spinboxes to be edited
   */
//  void on_vertex_spinbox_valueChanged(int arg1);

  /*
   * Saves the new vertex coordinates to the Mesh object and refreshes the view
   */

  void on_move_vertex_button_clicked();

  /*
   * Adds a Vertex to the Mesh at the selected coordinates
   */
  void on_Add_Vertex_pushButton_clicked();


  /*
   * Deletes the currently active vertex
   */
  void on_Delete_Vertex_pushButton_clicked();

  /*
   * Adds a link to the Mesh between the two given vertices
   */

  void on_Add_Link_pushButton_clicked();

  /*
   * Deletes the link between the two given vertices
   */

  void on_Delete_Link_pushButton_clicked();

  ///////////////////////////////////////////
  // View functions
  ///////////////////////////////////////////


  /*
   * Saves new scale factor to scalefactor
   */
//  void on_zoom_factor_doubleSpinBox_valueChanged(double arg1);


//  void on_horizontalSlider_sliderMoved(int position);


  /*
   * Sets the zoom spinbox to the given scalefactor
   */
  void updateZoomSpinbox(double scaleFactor);

  void on_Rotation_slider_sliderMoved(int position);


  ///////////////////////////////////////////
  // Transformations functions
  ///////////////////////////////////////////

  void on_Save_Transformation_pushButton_clicked();

  ///////////////////////////////////////////
  // Changelog functions
  ///////////////////////////////////////////

  void undoMovement(Movement selectedAction);

  void redoMovement(Movement selectedAction);

  void on_Clear_History_pushButton_clicked();

  void on_Undo_Change_pushButton_clicked();

  void on_Redo_PushButton_clicked();

  void on_Undo_pushButton_clicked();

  void on_Change_ViewMode_pushButton_clicked();

  void on_horizontalSlider_valueChanged(int value);

  void on_Rotate_Selection_slider_valueChanged(int value);

  void initialise_transformation();

  void applyTransformation();


  void on_Scale_Selection_doubleSpinBox_valueChanged(double arg1);

  void on_zoom_factor_doubleSpinBox_valueChanged(double arg1);

  void on_checkBox_clicked();

  void on_checkBox_2_clicked();

private:
  Ui::SideBar *ui;
  QGraphicsItemGroup *transformationGroup = nullptr;
  QTransform rotationMat;
  QTransform scaleMat;
};

#endif // SIDEBAR_H
