#include "UI/sidebar.h"
#include "ui_sidebar.h"

#include <iostream>
#include <limits>

#include "UI/mainwindow.h"
#include "Mesh/vertex.h"
#include "Mesh/Fields/thresholdfield.h"

SideBar::SideBar(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::SideBar)
{
  ui->setupUi(this);

  connect(this, SIGNAL(newMeshLoaded(Mesh*)), ui->Changelog_tableView, SLOT(reinitialiseTable(Mesh*)));
  connect(this, SIGNAL(addVertexMovement(int)), ui->Changelog_tableView, SLOT(addVertexMovement(int)));
  connect(this, SIGNAL(addVertexInChangelog(int)), ui->Changelog_tableView, SLOT(addVertexInChangelog(int)));
  connect(this, SIGNAL(deleteVertexInChangelog(int)), ui->Changelog_tableView, SLOT(deleteVertexInChangelog(int)));
  connect(this, SIGNAL(addEdgeInChangelog(int, int)), ui->Changelog_tableView, SLOT(addEdgeInChangelog(int, int)));
  connect(this, SIGNAL(deleteEdgeInChangelog(int, int)), ui->Changelog_tableView, SLOT(deleteEdgeInChangelog(int, int)));
}

SideBar::~SideBar()
{
  delete ui;
}

///////////////////////////////////////////
// Main
///////////////////////////////////////////

void SideBar::refreshVertexCoords(int vertexNumber){
  Vertex* movingVertex = qobject_cast<MainWindow*>(window())->getActiveMesh()->getVertexFromNumber(vertexNumber);
  ui->vertex_spinbox->setValue(vertexNumber);
  ui->x_coordinate_doubleSpinBox->setValue(movingVertex->x());
  ui->y_coordinate_doubleSpinBox->setValue(movingVertex->y());
}

void SideBar::on_move_vertex_button_clicked()
{
  if (qobject_cast<MainWindow*>(window())->getActiveMesh()){
    int vertexNumber = ui->vertex_spinbox->value();
    double newX = ui->x_coordinate_doubleSpinBox->value();
    double newY = ui->y_coordinate_doubleSpinBox->value();
    Vertex* movedVertex = qobject_cast<MainWindow*>(window())->getActiveMesh()->getVertexFromNumber(vertexNumber);
    movedVertex->moveVertex(newX,newY,true);
  }
}

void SideBar::on_Add_Vertex_pushButton_clicked()
{
  Mesh* mesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
  int vertNumber = mesh->vertexCount() + 1;
  double xPosition = ui->x_coordinate_doubleSpinBox->value();
  double yPosition = ui->y_coordinate_doubleSpinBox->value();
  Vertex* newVertex = new Vertex(vertNumber,xPosition,yPosition,0, mesh);
  mesh->addVertex(newVertex);
  emit meshSizeChanged(mesh->vertexCount());
  updateMeshInfo();
}

void SideBar::on_Add_Link_pushButton_clicked()
{
  int vertexNumber2 = ui->Link_Start_SpinBox->value();
  int vertexNumber1 = ui->Link_End_spinBox->value();
  int startVertex = std::min(vertexNumber1, vertexNumber2);
  int endVertex = std::max(vertexNumber1, vertexNumber2);
  qobject_cast<MainWindow*>(window())->getActiveMesh()->addLine(startVertex, endVertex);
  updateMeshInfo();
}

void SideBar::on_Delete_Link_pushButton_clicked()
{
  int startVertNumber = ui->Link_Start_SpinBox->value();
  int endVertNumber = ui->Link_End_spinBox->value();
  if(startVertNumber != endVertNumber){
    Mesh* mesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
    Vertex* startVertex = mesh->getVertexFromNumber(startVertNumber);
    Vertex* endVertex = mesh->getVertexFromNumber(endVertNumber);
    mesh->deleteLine(startVertex, endVertex);
  }
  updateMeshInfo();
}

void SideBar::on_Delete_Vertex_pushButton_clicked()
{
  for (QGraphicsItem* p : qobject_cast<MainWindow*>(window())->getMeshView()->scene()->selectedItems()){
    Vertex* test = qgraphicsitem_cast<Vertex*>(p);
    test->parentMesh->deleteVertex(test);
  }
  updateMeshInfo();
}

void SideBar::on_Change_ViewMode_pushButton_clicked()
{
  emit cycleViewMode();
}


///////////////////////////////////////////
// View
///////////////////////////////////////////

void SideBar::updateZoomSpinbox(double scaleFactor){
  ui->zoom_factor_doubleSpinBox->setValue(scaleFactor);
}

void SideBar::on_zoom_factor_doubleSpinBox_valueChanged(double arg1)
{
 qobject_cast<MainWindow*>(window())->getMeshView()->zoom(arg1);
}

void SideBar::on_Rotation_slider_sliderMoved(int position){
  emit setViewRotation(position);
}

///////////////////////////////////////////
// Transformations
///////////////////////////////////////////

void SideBar::initialise_transformation(){
  if(transformationGroup){
    qobject_cast<MainWindow*>(window())->getMeshView()->scene()->destroyItemGroup(transformationGroup);
    transformationGroup = nullptr;
    return;
  }
  transformationGroup = qobject_cast<MainWindow*>(window())->getMeshView()->scene()->createItemGroup(qobject_cast<MainWindow*>(window())->getMeshView()->scene()->selectedItems());

  //Form a bounding rectangle of the points in the selection
  //We then take the centre point of this to be the origin
  double xMin = std::numeric_limits<double>::max();
  double xMax = std::numeric_limits<double>::lowest();
  double yMin = std::numeric_limits<double>::max();
  double yMax = std::numeric_limits<double>::lowest();
  for (Vertex* vert : qobject_cast<MainWindow*>(window())->getMeshView()->selectedVertices){
      xMin = std::min(xMin, vert->x());
      xMax = std::max(xMax, vert->x());
      yMin = std::min(yMin, vert->y());
      yMax = std::max(yMax, vert->y());
  }
  QPointF centre = QPointF((xMin+xMax)/2,(yMin+yMax)/2);

  transformationGroup->setTransformOriginPoint(centre);

}

void SideBar::on_Scale_Selection_doubleSpinBox_valueChanged(double scalefactor)
{
  //Form a bounding rectangle of the points in the selection
  //We then take the centre point of this to be the origin
  double xMin = std::numeric_limits<double>::max();
  double xMax = std::numeric_limits<double>::lowest();
  double yMin = std::numeric_limits<double>::max();
  double yMax = std::numeric_limits<double>::lowest();
  for (Vertex* vert : qobject_cast<MainWindow*>(window())->getMeshView()->selectedVertices){
      xMin = std::min(xMin, vert->x());
      xMax = std::max(xMax, vert->x());
      yMin = std::min(yMin, vert->y());
      yMax = std::max(yMax, vert->y());
  }
  QPointF centre = QPointF((xMin+xMax)/2,(yMin+yMax)/2);

  scaleMat = QTransform().translate(-centre.x(), -centre.y()) * QTransform::fromScale(scalefactor, scalefactor) * QTransform().translate(centre.x(), centre.y());
  applyTransformation();
}

void SideBar::on_Rotate_Selection_slider_valueChanged(int value)
{
  //Form a bounding rectangle of the points in the selection
  //We then take the centre point of this to be the origin
  double xMin = std::numeric_limits<double>::max();
  double xMax = std::numeric_limits<double>::lowest();
  double yMin = std::numeric_limits<double>::max();
  double yMax = std::numeric_limits<double>::lowest();
  for (Vertex* vert : qobject_cast<MainWindow*>(window())->getMeshView()->selectedVertices){
      xMin = std::min(xMin, vert->x());
      xMax = std::max(xMax, vert->x());
      yMin = std::min(yMin, vert->y());
      yMax = std::max(yMax, vert->y());
  }
  QPointF centre = QPointF((xMin+xMax)/2,(yMin+yMax)/2);

  rotationMat = QTransform().translate(-centre.x(), -centre.y()) * QTransform().rotate(value) * QTransform().translate(centre.x(), centre.y());
  applyTransformation();
}

void SideBar::applyTransformation(){
  transformationGroup->setTransform(scaleMat * rotationMat);

  for (QGraphicsItem* item : transformationGroup->childItems()){
    dynamic_cast<Vertex*>(item)->updateLines();
  }
}

void SideBar::on_Save_Transformation_pushButton_clicked()
{
  initialise_transformation();
}

///////////////////////////////////////////
// Mesh Info
///////////////////////////////////////////

void SideBar::updateMeshInfo(){
  Mesh* currentMesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
  ui->lineEdit_1->setText(QString(std::to_string(currentMesh->vertexCount()).c_str()));
  ui->lineEdit_2->setText(QString(std::to_string(currentMesh->lineCount()).c_str()));
  ui->lineEdit_3->setText(QString(std::to_string(currentMesh->triCount()).c_str()));
  ui->vertex_spinbox->setMinimum(1);
  ui->vertex_spinbox->setMaximum(currentMesh->vertexCount());
  ui->Link_Start_SpinBox->setMaximum(currentMesh->vertexCount());
  ui->Link_End_spinBox->setMaximum(currentMesh->vertexCount());
}


///////////////////////////////////////////
// Changelog functions
///////////////////////////////////////////

void SideBar::undoChain(){
  if (qobject_cast<MainWindow*>(window())->getActiveMesh() && ui->Changelog_tableView->model()->rowCount() > 0 ){
    //Finds index of currently selected row
    QModelIndex selectedRowIndex = ui->Changelog_tableView->currentIndex();
    int row;
    //Check if no row was selected
    if (!selectedRowIndex.isValid()){
      row = ui->Changelog_tableView->model()->rowCount()-1;
    }
    else{
      row = selectedRowIndex.row();
    }
    Movement selectedMovement = qobject_cast<VertexChangelog*>(ui->Changelog_tableView->model())->retrieveMovement(row);
    undoMovement(selectedMovement);
    ui->Changelog_tableView->selectRow(row-1);
 }

}


void SideBar::redoChain(){
  if (qobject_cast<MainWindow*>(window())->getActiveMesh() && ui->Changelog_tableView->model()->rowCount() > 0 ){
    //Finds index of currently selected row
    QModelIndex selectedRowIndex = ui->Changelog_tableView->currentIndex();
    int row;
    //Check if no row was selected
    if (selectedRowIndex.isValid()){
      row = selectedRowIndex.row();
      Movement selectedMovement = qobject_cast<VertexChangelog*>(ui->Changelog_tableView->model())->retrieveMovement(row);
      redoMovement(selectedMovement);
      ui->Changelog_tableView->selectRow(row+1);
    }
  }
}

void SideBar::on_Undo_Change_pushButton_clicked()
{
  //Finds index of currently selected row
  QModelIndex selectedRowIndex = ui->Changelog_tableView->currentIndex();
  Mesh* currentMesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
  //Check if no row was selected
  if (selectedRowIndex.isValid()){
    int row = selectedRowIndex.row();
    Movement selectedMovement = qobject_cast<VertexChangelog*>(ui->Changelog_tableView->model())->retrieveMovement(row);
    undoMovement(selectedMovement);
  }

}

void SideBar::undoMovement(Movement selectedAction){
  Mesh* currentMesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
  switch (selectedAction.getActionType()){
  case Movement::vertexMovement:
    //Undoes the action of moving a vertex

    currentMesh->getVertexFromNumber(selectedAction.vertexNumber)->moveVertex(selectedAction.oldPos, false);
    break;

  case Movement::addVertex:
    //Undoes the action of adding a new vertex
    currentMesh->deleteVertex(currentMesh->getVertexFromNumber(selectedAction.vertexNumber), false);
    break;

  case Movement::deleteVertex:
  {
    //Undoes the action of deleting a vertex

    Vertex* deletedVertex = currentMesh->getVertexFromNumber(selectedAction.vertexNumber);
    //Hides the vertex so when it is removed from the scene it's appearance isn't left behind.
    deletedVertex->setVisible(true);

    //Remove Vertex from scene
    qobject_cast<MainWindow*>(window())->getMeshView()->scene()->addItem(deletedVertex);

    //Cannot remove vertex from Mesh yet as would require renumbering all vertices
    deletedVertex->setDeletionMarker(false);
    break;
  }

  case Movement::addEdge:
    //Undoes the action of adding an edge
    currentMesh->deleteLine(selectedAction.startVertexNumber, selectedAction.endVertexNumber, false);
    break;

  case Movement::deleteEdge:
    //Undoes the action of deleting an edge
    currentMesh->addLine(selectedAction.startVertexNumber, selectedAction.endVertexNumber, false);
    break;
  }

}

void SideBar::redoMovement(Movement selectedAction){
  Mesh* currentMesh = qobject_cast<MainWindow*>(window())->getActiveMesh();
  switch (selectedAction.getActionType()){
  case Movement::vertexMovement:
    //Undoes the action of moving a vertex

    currentMesh->getVertexFromNumber(selectedAction.vertexNumber)->moveVertex(selectedAction.newPos, false);
    break;

  case Movement::addVertex:
    {
      //Undoes the action of deleting a vertex

      Vertex* deletedVertex = currentMesh->getVertexFromNumber(selectedAction.vertexNumber);
      //Hides the vertex so when it is removed from the scene it's appearance isn't left behind.
      deletedVertex->setVisible(true);

      //Remove Vertex from scene
      qobject_cast<MainWindow*>(window())->getMeshView()->scene()->addItem(deletedVertex);

      //Cannot remove vertex from Mesh yet as would require renumbering all vertices
      deletedVertex->setDeletionMarker(false);
      break;
      }

  case Movement::deleteVertex:
    //Undoes the action of adding a new vertex
    currentMesh->deleteVertex(currentMesh->getVertexFromNumber(selectedAction.vertexNumber), false);
    break;

  case Movement::addEdge:
    //Undoes the action of adding an edge
    currentMesh->addLine(selectedAction.startVertexNumber, selectedAction.endVertexNumber, false);
    break;

  case Movement::deleteEdge:
    //Undoes the action of deleting an edge
    currentMesh->deleteLine(selectedAction.startVertexNumber, selectedAction.endVertexNumber, false);
    break;
  }

}


void SideBar::on_Clear_History_pushButton_clicked()
{
  qobject_cast<VertexChangelog*>(ui->Changelog_tableView->model())->clearList();
  qobject_cast<MainWindow*>(window())->getActiveMesh()->collapseVertexVector();
}

void SideBar::on_Redo_PushButton_clicked()
{
  redoChain();
}


void SideBar::on_Undo_pushButton_clicked()
{
  undoChain();
}

///////////////////////////////////////////
// Fields
///////////////////////////////////////////

FieldEditor* SideBar::getFieldEditor(){
  return ui->Field_Editor;
}

///////////////////////////////////////////
// Freeform Editing
///////////////////////////////////////////

QString SideBar::getPrependText(){
  return ui->Prepend_Text_textEdit->toPlainText();
}

QString SideBar::getAppendText(){
  return ui->Append_Text_textEdit->toPlainText();
}

void SideBar::writeUnknownText(QString text){
  ui->Append_Text_textEdit->append(text);
}

void SideBar::on_horizontalSlider_valueChanged(int value)
{
  qobject_cast<MainWindow*>(window())->getMeshView()->getActiveMesh()->updateVertexRadius(double(value));
  qobject_cast<MainWindow*>(window())->getMeshView()->refreshView();
}


void SideBar::on_checkBox_clicked()
{
  qobject_cast<MainWindow*>(window())->getMeshView()->verticalFlip();
}

void SideBar::on_checkBox_2_clicked()
{
  qobject_cast<MainWindow*>(window())->getMeshView()->horizontalFlip();
}
