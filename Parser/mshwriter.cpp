#include "Parser/mshwriter.h"

#include <iostream>
#include <fstream>
#include <vector>

#include "Mesh/mesh.h"

MshWriter::MshWriter()
{

}

MshWriter::~MshWriter(){
  delete file;
}

bool MshWriter::write(std::string savePath, Mesh* mesh){

  file = new std::ofstream(savePath.c_str());

  if (!file->is_open()){
    std::cout << "couldn't write to file" << std::endl;
    }
  else{
    *file << "$MeshFormat\n2.2 0 8\n$EndMeshFormat" << std::endl;
    writeVertices(mesh);
    writeElements(mesh);

    return true;
  }
  return false;
}

void MshWriter::writeVertices(Mesh* mesh){
  *file << "$Nodes" << std::endl;
  *file << mesh->vertexCount() << std::endl;
  for (Vertex* vert : mesh->getVertices()){
    *file << std::to_string(vert->vertexNumber) << " " << std::to_string(vert->x()) << " " << std::to_string(vert->y()) << " 0" << std::endl;
  }
  *file << "$EndNodes" << std::endl;
}

void MshWriter::writeElements(Mesh* mesh){
  *file << "$Elements" << std::endl;
  *file << mesh->elementsText;
  *file << "$EndElements";
}
