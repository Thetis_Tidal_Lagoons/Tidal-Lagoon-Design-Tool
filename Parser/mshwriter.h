#ifndef MSHWRITER_H
#define MSHWRITER_H

#include <string>

class Mesh;

class MshWriter
{
public:
  MshWriter();
  ~MshWriter();

  std::ofstream* file = nullptr;

  bool write(std::string savePath, Mesh* mesh);
  void writeVertices(Mesh* mesh);
  void writeElements(Mesh* mesh);



};

#endif // MSHWRITER_H
