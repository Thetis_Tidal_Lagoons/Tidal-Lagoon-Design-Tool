#include "Mesh/Fields/thresholdfield.h"

#include "Parser/geowriter.h"

ThresholdField::ThresholdField(): Field()
{

}

ThresholdField::ThresholdField(QString fieldName, int newDistMax, int newDistMin, int newIField, int newLcMax, int newLcMin, bool newSigmoid, bool newStopAtDistMax, bool backgroundField) : Field(fieldName, backgroundField){
  DistMax = newDistMax;
  DistMin = newDistMin;
  IField = newIField;
  LcMax = newLcMax;
  LcMin = newLcMin;
  Sigmoid = newSigmoid;
  StopAtDistMax = newStopAtDistMax;
}


QString ThresholdField::printFieldInfo(int fieldIndex, GeoWriter* writer){

  Q_UNUSED(writer)
  QString fieldParameters;
  QString fieldIdentifier = "Field[IF + " + QString::number(fieldIndex) + "]";

  //Define type of field
  fieldParameters.append(fieldIdentifier + " = Threshold;\n");
  fieldParameters.append(fieldIdentifier + ".DistMax = "+ QString::number(DistMax) +";\n");
  fieldParameters.append(fieldIdentifier + ".DistMin = "+ QString::number(DistMin) +";\n");
  fieldParameters.append(fieldIdentifier + ".IField = "+ QString::number(IField) +";\n");
  fieldParameters.append(fieldIdentifier + ".LcMax = "+ QString::number(LcMax) +";\n");
  fieldParameters.append(fieldIdentifier + ".LcMin = "+ QString::number(LcMin) +";\n");
  fieldParameters.append(fieldIdentifier + ".Sigmoid = "+ QString::number(Sigmoid) +";\n");
  fieldParameters.append(fieldIdentifier + ".StopAtDistMax = "+ QString::number(StopAtDistMax) +";\n");


  return fieldParameters;
}
