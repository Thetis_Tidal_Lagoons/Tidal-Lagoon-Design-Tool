#ifndef MESH_H
#define MESH_H

#include <vector>
#include "vertex.h"
#include "meshedge.h"
#include "spline.h"
#include "meshview.h"

#include "physicalgroup.h"

class Vertex;
class MeshEdge;
class Spline;

class Field;
class FieldEditor;
class MeshView;

class Mesh : public QObject
{
  Q_OBJECT

public:
  Mesh();

  Mesh(MeshView* view);

  ~Mesh();

  ///////////////////////////////////////////
  // Member variables
  ///////////////////////////////////////////

  int meshType = 0;
  enum { undefined = 0 };
  enum { geo = 1 };
  enum { msh = 2 };

  std::vector<Spline*> lineGroupList;
  std::vector<Spline*> splineList;
  FieldEditor* fieldEditor = nullptr;

  //Leftovers from .msh file
  //Will need to be edited if add/removing lines
  std::string elementsText;

  std::string fileLocation;

  ///////////////////////////////////////////
  // Mesh I/O functions
  ///////////////////////////////////////////

  /*
   * Opens a file dialog and saves the mesh to the chosen file
   */
  bool saveMesh(QString savePath);

  /*
   * Saves the mesh into a .geo file
   */
  bool saveMeshAsGeo(QString savePath);

  /*
   * Saves the mesh into a .msh (format 2) file
   */
  bool saveMeshAsMsh(QString savePath);

  ///////////////////////////////////////////
  // Mesh Information
  ///////////////////////////////////////////

  /*
   * Returns the number of vertices in the mesh
   */
  int vertexCount();

  /*
   * Returns the number of external and internal lines in the mesh
   */
  int lineCount();

  /*
   * Returns the number of triangles in the mesh
   */
  int triCount();

  ///////////////////////////////////////////
  // "Get" functions
  ///////////////////////////////////////////

  /*
   * Returns the vertex at given index in vertices vector
   * Note: This has a offset of -1 from the vertex number in the .msh file
   */
  Vertex* getVertexFromIndex(unsigned int index);

  Vertex* getVertexFromNumber(unsigned int number);

  std::shared_ptr<MeshEdge> getLineFromIndex(unsigned int index);

  std::shared_ptr<MeshEdge> getLineFromNumber(unsigned int number);

  /*
   * Returns the vector of vectors containing the positions of vertices
   */
  std::vector<Vertex*> getVertices();

  /*
   * Returns the vector of vectors containing the connections in the mesh
   */
  std::vector<std::shared_ptr<MeshEdge> > getLines();

  ///////////////////////////////////////////
  // Vertex appearance functions
  ///////////////////////////////////////////

  double vertexRadius = 6;

  void updateVertexRadius(double circleRadius);

  ///////////////////////////////////////////
  // MeshEdge appearance
  ///////////////////////////////////////////

  bool hiddenFlag = false;
  bool isHidden();
  void setHidden(bool hiddenStatus);

  ///////////////////////////////////////////
  // Physical Groups
  ///////////////////////////////////////////

  std::vector<PhysicalGroup*> physGroups;
  /*
   * Adds the given MeshElement to the group with name groupName
   * If no suitable group exists, one is created
   */
  bool addToPhysGroup(MeshElement* item, QString groupName);

  /*
   * Removes the given MeshElement from group with name groupName
   * If no groupName is given then MeshElement is removed from all groups
   * If a group is reduced to 0 MeshElements, it is deleted
   */
  bool removeFromPhysGroup(MeshElement* item, QString groupName = QString());

  /*
   * Purges vertices which are marked for deletion and re-numbers the remaining
   * Called when edit history is deleted as the marked vertices can then no longer be recovered
   */
  bool collapseVertexVector();

  ///////////////////////////////////////////
  // Misc functions
  ///////////////////////////////////////////

  /*
   * Test for if Mesh contains a valid mesh
   * No vertices == no mesh
   */
  explicit operator bool() const{
      return !vertices.empty();
  }

private:
  //Vertices in the mesh
  std::vector<Vertex*> vertices;

  //Lines of the mesh
  std::vector<std::shared_ptr<MeshEdge> > meshLines;

signals:

  /*
   * Signifies a change in the mesh size so that limits on spinboxes, etc can be updated
   */
  void meshSizeChanged(int vertexCount);

  /*
   * Signifies elements being added or deleted from mesh
   */
  void vertexAdded(int vertexNumber);

  void vertexDeleted(int vertexNumber);

  void edgeAdded(int startVertex, int endVertex);

  void edgeDeleted(int startVertex, int endVertex);

  /*
   * Signifies that a quantity which the progress bar is tracking must be updated.
   */
  void updateProgressBar(double percentage);

public slots:

  ///////////////////////////////////////////
  // Vertex Addition/Deletion
  ///////////////////////////////////////////

  /*
   * Adds a vertex as position (x,y,z) to the Mesh object
   */
  void addVertex(Vertex*, bool trackChange = true);

  void addVertex(QPointF position, bool trackChange = true);

  /*
   * Removes the given vertex from the Mesh and deletes it
   */
  bool deleteVertex(Vertex* delVertex, bool trackChange = true);

  ///////////////////////////////////////////
  // Line Addition/Deletion
  ///////////////////////////////////////////


  /*
   * Adds a MeshEdge between the two given vertices
   */
  void addLine(Vertex* startVertex, Vertex* endVertex, bool trackChange = true);

  void addLine(int startVertexNumber, int endVertexNumber, bool trackChange = true);

  /*
   * Deletes the line at the address of the given pointer
   */
  bool deleteLine(std::shared_ptr<MeshEdge> delLine, bool trackChange = true);

  /*
   * Tests whether a MeshEdge between the two given vertices exists
   * and deletes it if so.
   */
  bool deleteLine(Vertex* startVertex, Vertex* endVertex, bool trackChange = true);

  /*
   * Tests whether a MeshEdge between the two vertices described by given vertex numbers exists
   * and deletes it if so.
   */
  bool deleteLine(int startVertexNumber, int endVertexNumber, bool trackChange = true);

  ///////////////////////////////////////////
  // LineGroup Addition/Deletion
  ///////////////////////////////////////////


  /*
   * Attempts to construct a LineGroup between rootVertex and targetVertex
   * Returns whether a new LineGroup is constructed successfully
   */
  bool addLineGroup(Vertex* rootVertex, Vertex* targetVertex, QString physicalGroupName = QString());

  /*
   * Attempts to construct a LineGroup between vertices given by rootVertexNumber and targetVertexNumber
   * Returns whether a new LineGroup is constructed successfully
   */
  bool addLineGroup(int rootVertexNumber, int targetVertexNumber, QString physicalGroupName = QString());

  /*
   * Takes a given path of vertices and constructs a LineGroupfrom this
   * Returns whether a new LineGroup is constructed successfully
   */
  bool addLineGroup(std::vector<Vertex*> vertexList, QString physicalGroup = QString());

  /*
   * Deletes the given LineGroup
   */
  bool deleteLineGroup(Spline* delSpline);

  ///////////////////////////////////////////
  // Spline Addition/Deletion
  ///////////////////////////////////////////

  /*
   * Attempts to construct a Spline between vertices given by rootVertexNumber and targetVertexNumber
   * Returns whether a new Spline is constructed successfully
   *
   * Note: Calls addSpline(rootvertex, targetVertex, physicalGroupName)
   */
  bool addSpline(int rootVertexNumber, int targetVertexNumber, QString physicalGroupName = QString());

  /*
   * Attempts to construct a Spline between rootVertex and targetVertex
   * Returns whether a new Spline is constructed successfully
   *
   * Note: Calls addSpline(vertexList, physicalGroupName)
   */
  bool addSpline(Vertex* rootVertex, Vertex* targetVertex, QString physicalGroupName = QString());

  /*
   * Takes a given path of vertices and constructs a Spline from this
   * Returns whether a new Spline is constructed successfully
   */
  bool addSpline(std::vector<Vertex*> vertexList, QString physicalGroup = QString());

  /*
   * Deletes the given Spline
   */
  bool deleteSpline(Spline* delSpline);

};


bool sortRule(const std::vector<int>& a, const std::vector<int>& b);

#endif // MESH_H
