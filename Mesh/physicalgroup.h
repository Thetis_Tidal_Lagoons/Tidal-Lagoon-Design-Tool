#ifndef PHYSICALGROUP_H
#define PHYSICALGROUP_H

#include <vector>

#include <QString>

class MeshElement;

class PhysicalGroup
{
public:

  ///////////////////////////////////////////
  // Constructors
  ///////////////////////////////////////////

  PhysicalGroup();
  PhysicalGroup(QString name, MeshElement* templateElement);

  ///////////////////////////////////////////
  // Data structures
  ///////////////////////////////////////////

  // A useful name to describe the physical meaning of the group
  QString groupName;

  //Flag to distinguish points from lines (or from surfaces)
  int elementType;

  //Vector of the elements included in the group
  std::vector<MeshElement*> elementList;

};

#endif // PHYSICALGROUP_H
