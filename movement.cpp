#include "movement.h"


Movement::Movement(){

}

Movement::Movement(int vertexNo, QPointF oldPosition, QPointF newPosition){
  //Represents a movement
  actionType = vertexMovement;
  vertexNumber = vertexNo;
  oldPos = oldPosition;
  newPos = newPosition;
}

Movement::Movement(int vertexNo, QPointF position, int action = Movement::addVertex){
  actionType = action;
  vertexNumber = vertexNo;
  oldPos = position;
}

Movement::Movement(int startVertex, int endVertex, int action = Movement::addEdge){
  actionType = action;
  startVertexNumber = startVertex;
  endVertexNumber = endVertex;
}

int Movement::getActionType(){
  return actionType;
}

